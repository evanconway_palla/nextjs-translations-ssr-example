/*
Syntax explained here:
https://stackoverflow.com/questions/44497388/typescript-array-to-string-literal-type
*/
export const localeKey = "locale";
export const locales = ["en", "fr", "es"] as const;
export type Locale = (typeof locales)[number];

export const localeDefault = locales[0];

interface Copy {
  index: {
    title: string;
  };
}

const messages: Record<Locale, Copy> = {
  en: {
    index: {
      title: "Hello world!",
    },
  },
  es: {
    index: {
      title: "¡Hola Mundo!",
    },
  },
  fr: {
    index: {
      title: "Bonjour le monde!",
    },
  },
};

const getMessagesUtil = (locale: Locale) => {
  if (locale === "en") return messages.en;
  if (locale === "es") return messages.es;
  if (locale === "fr") return messages.fr;
  return messages.en;
};

const getMessagesFromLocale = (locale: Locale) => {
  return getMessagesUtil(locale);
};

export default getMessagesFromLocale;
