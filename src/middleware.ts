import { NextRequest, NextResponse } from "next/server";

export async function middleware(req: NextRequest) {
  const res = NextResponse.next();

  // setup locale cookie
  if (req.cookies.get("locale")?.value === undefined) {
    res.cookies.set("locale", "en");
  }

  return res;
}
