import { cookieGet } from "@/actions";

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang={await cookieGet("locale")}>
      <body>{children}</body>
    </html>
  );
}
