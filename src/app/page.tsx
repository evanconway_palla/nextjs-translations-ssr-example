import SetLocaleButton from "@/components/SetLocaleButton";
import getMessagesFromLocale, { locales } from "../../messagesJS";
import { cookieGetLocale } from "@/actions";

export default async function Home() {
  const locale = await cookieGetLocale();
  const text = getMessagesFromLocale(locale).index.title;
  return (
    <div>
      <div>{locale}</div>
      <div>{text}</div>
      <ul>
        {locales.map((lang) => (
          <li key={lang}>
            <SetLocaleButton localetoSetTo={lang} />
          </li>
        ))}
      </ul>
    </div>
  );
}
