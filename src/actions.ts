"use server";

import { cookies } from "next/headers";
import { Locale, localeDefault, localeKey } from "../messagesJS";

type CookieKey = "locale";

export async function cookieGet(key: CookieKey) {
  return cookies().get(key)?.value;
}

export async function cookieSet(key: CookieKey, value: any) {
  cookies().set(key, value);
}

export async function cookieGetLocale() {
  const value = cookies().get(localeKey)?.value;
  if (value === undefined) return localeDefault;
  return value as Locale;
}

export async function cookieSetLocale(locale: Locale) {
  cookieSet(localeKey, locale);
}
