import { cookieSetLocale } from "@/actions";
import { Locale } from "../../messagesJS";

const SetLocaleButton = ({ localetoSetTo }: { localetoSetTo: Locale }) => {
  return (
    <form
      action={async () => {
        "use server";
        await cookieSetLocale(localetoSetTo);
      }}
    >
      <button type="submit">{localetoSetTo}</button>
    </form>
  );
};

export default SetLocaleButton;
